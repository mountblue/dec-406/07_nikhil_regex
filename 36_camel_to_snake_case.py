import re

input = 'letWwalkFonRoad.'

result = re.sub(r'[A-Z]', lambda x: '_' + x.group(0).lower(), input)

print(result)