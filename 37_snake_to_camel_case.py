import re

input = 'let_walk_on_road.'

result = re.sub(r'_([a-z])', lambda x: x.group(1).upper(), input)

print(result)