import re

inputs = ["example (.com)", "w3resource", "github (.com)", "stackoverflow (.com)"]

for input in inputs:
    result = re.sub(r' ?\([^)]+\)', "", input)
    print(result)