import re

input = input()

result = re.sub(r' ',':', input, count=2)
result = re.sub(r'\.',':', result, count=2)
result = re.sub(r',',':', result, count=2)

print(result)