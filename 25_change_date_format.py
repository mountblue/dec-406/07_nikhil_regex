import re

input = '2018-12-28'

result = re.sub(r'(\d{4})-(\d{2})-(\d{2})', '\\3-\\2-\\1', input)

print(result)