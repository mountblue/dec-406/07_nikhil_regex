import re

input_string = "Welcome to this World"
replace_from = "world"
replace_to = "island"
string_compiled = re.compile(replace_from, re.IGNORECASE)
result = string_compiled.sub(replace_to, input_string)
print(result)