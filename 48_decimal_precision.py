import re

input = "1.45"
result = re.findall(r'^[0-9]+(\.[0-9]{1,2})?$', input)

if result:
    print(result)
else: print("No match")