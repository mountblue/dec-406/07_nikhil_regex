import re

string = "jndsan2004/12/12"

pattern = re.compile(r'(\d{4})[.-/](\d{2})[./-](\d{2})')
results = re.finditer(pattern, string)

for result in results:
    print(result.group())
