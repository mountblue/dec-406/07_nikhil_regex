import re

input_text = "asd#$@"

result = re.findall(r'[^a-zA-Z0-9]', input_text)

if result:
    print("contains special char")
else:
    print("only alphanum")
