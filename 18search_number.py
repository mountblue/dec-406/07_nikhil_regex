import re

input_text = "dsa5551sad1315dsa21"

result = re.findall(r'\d{1,3}', input_text)

if result:
    print(result)
else:
    print("only alphanum")
