import re

input_text = "Python exercises, PHP exercises, C# exercises"

results = re.finditer(r'\bP\w+', input_text)

for result in results:
    print(result.group())