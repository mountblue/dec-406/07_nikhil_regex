import re

input  = "Clearly, he has no excuse for such behavior."

results = re.finditer(r"\w+ly", input)
for result in results:
    print(result.group(0), result.start())