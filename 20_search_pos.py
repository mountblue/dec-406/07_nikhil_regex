import re

input_text = "The quick brown fox jumps over the lazy dog."
search = "dog"
result = re.search(search, input_text)

if result:
    print(result.group(), result.start())
